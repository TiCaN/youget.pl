#!/usr/bin/perl

use strict;
use utf8;

use open ":encoding(utf-8)";
binmode (STDOUT, ":utf8");

$/ = $\ = "\n";

my @afmts, my @vfmts;

sub download
{
  my $link = shift;

  $link =~ /v=([a-zA-Z0-9_-]*)/;
  $link = "https://youtube.com/watch?v=$1";
  system "wget $link -O /tmp/youtuber/html";
  my $text;
  open html, "</tmp/youtuber/html" or die "failed wget!";
  {
    undef local $/;
    $text = <html>;
  }
  close html;
  $text =~ m!<title>(.*?) - YouTube</title>!mg;
  my $filename = $1;

  my $fmts = `youtube-dl -F $link`;
  my ($targeta, $targetv, $aext, $vext);

  ASEARCH: for my $testa (@afmts)
  {
    if($fmts =~ /$testa *(\S*)/m)
    {
      $targeta = $testa;
      $aext = $1;
      last ASEARCH;
    }
  }
  VSEARCH: for my $testv (@vfmts)
  {
    if($fmts =~ /$testv *(\S*)/m)
    {
      $targetv = $testv;
      $vext = $1;
      last VSEARCH;
    }
  }
  chdir "$ENV{HOME}/.youtuber/video/";
  system "youtube-dl --external-downloader aria2c --external-downloader-args \"--split 16 --min-split-size 1M --max-connection-per-server 16 --dir=$ENV{HOME}/.youtuber/video -o audio.$aext\" -f $targeta -o $ENV{HOME}/.youtuber/video/audio.$aext $link";
  system "youtube-dl --external-downloader aria2c --external-downloader-args \"--split 16 --min-split-size 1M --max-connection-per-server 16 --dir=$ENV{HOME}/.youtuber/video -o audio.$vext\" -f $targetv -o $ENV{HOME}/.youtuber/video/video.$vext $link";
  system "ffmpeg -nostdin -i video.$vext -i audio.$aext -c copy \"$filename.mp4\"";
  unlink "video.$vext";
  unlink "audio.$aext";
  
  chdir "$ENV{HOME}/.youtuber/";
}

chdir "$ENV{HOME}/.youtuber/";
mkdir '/tmp/youtuber';

my @videos;
my $check = 0;
my $banall = 0;

for (@ARGV)
{
  if(/^-h|--help$/)
  {
    print "<ulrlist> будет скачивать видяшки в ~/.youtuber/video";
    print "запуск без параметров будет просматривать каналы";
    print "--check позволяет проверить конфигуратор каналов на правильность";
    print "--ban-all выключает все видео для сброса листа";
    exit 0;
  }
  elsif(/^--check$/)
  {
    $check = 1;
  }
  elsif(/^--ban-all$/)
  {
    $banall = 1;
  }
  elsif(/^-/)
  {
    print "неизвестный параметр";
    exit 1;
  }
  else
  {
    push @videos, $_;
  }
}

open prfa, "<$ENV{HOME}/.youtuber/prefera" or die '~/.youtuber/prefera?';
open prfv, "<$ENV{HOME}/.youtuber/preferv" or die '~/.youtuber/preferv?';
while(<prfa>)
{
  chomp;
  my @tmp = split / /;
  push @afmts, $tmp[0];
}
while(<prfv>)
{
  chomp;
  my @tmp = split / /;
  push @vfmts, $tmp[0];
}
close prfa;
close prfv;

if( scalar @videos )
{
  download $_ for (@videos);
  exit 0;
}

open channels, "<channels" or die "~/.youtuber/channels?";
my $channels;
{
  undef local $/;
  $channels = <channels>;
}
close channels;

my (%vidsw, %vidsb);

my $readed = '';
if(open readed, "<$ENV{HOME}/.youtuber/readed")
{
  undef local $/;
  $readed = <readed>;
  close readed;
}

while($channels =~ /^([a-zA-Z0-9]*).*?{(.*?)}/msg)
{
  my ($url, $set) = ($1, $2);
  my ($oignore, $name, @ban, @force) = (0,'unnamed',(),());
  my @settings = split /\n/, $set;
  for (@settings)
  {
    if(/options: (.*)/)
    {
      my $tmp = $1;
      $oignore = 1 if $tmp =~ /ignore/;
    }
    elsif(/name: (.*)/)
    {
      $name = $1;
    }
    elsif(/ban: (.*)/)
    {
      my $tmp = $1;
      while ($tmp =~ /<(.*?)>/g)
      {
        push @ban, $1;
      }
    }
    elsif(/force: (.*)/)
    {
      my $tmp = $1;
      while ($tmp =~ /<(.*?)>/g)
      {
        push @force, $1;
      }
    }
  }

  if($check)
  {
    print "----------";
    print "name: $name";
    print "url: $url";
    print "ignore: yes" if $oignore;
    print "ban:";
    print " $_" for @ban;
    print "force:";
    print " $_" for @force;
  } else {
    print "Test $name: $url as channel…";
    my $res = system "wget https://www.youtube.com/channel/$url/videos -O /tmp/youtuber/channel -q";
    if($res)
    {
      print "Test $name: $url as user…";
      $res = system "wget https://www.youtube.com/user/$url/videos -O /tmp/youtuber/channel -q";
    }
    if($res) {die "404: $name ($url)";}

    {
      open page, "<","/tmp/youtuber/channel" or die "File ran away";
      PAGE: while (<page>)
      {
        if(m!<a.*?title="(.*?)".*?href="(/watch.*?)".*?>!)
        {
          my($vname, $vlink) = ($1, $2);

          next PAGE if(-1 != index $readed, "https://youtube.com$vlink");

          my $status = $oignore; #0 — down, 1 — ban

          for(@ban)
          {
            $status = 1 if $vname =~ /$_/i;
          }

          for(@force)
          {
            $status = 0 if $vname =~ /$_/i;
          }

          $status = 1 if $banall;

          if($status)
          {
            $vidsb{"[$name] $vname"} = "https://youtube.com$vlink";
          } else {
            $vidsw{"[$name] $vname"} = "https://youtube.com$vlink";
          }
        }
      }
      close page;
    }
  }
}

my $counter;
my $trapper = scalar %vidsw + scalar %vidsb;
my $bstart;

while($trapper)
{
  my @list;

  print "load:";
  $counter = 0;
  for(keys %vidsw)
  {
    print "$counter. $_ -- ".$vidsw{$_};
    push @list, $_;
    $counter++;
  }

  $bstart = $counter;

  print "ban:";
  for(keys %vidsb)
  {
    print "$counter. $_ -- ".$vidsb{$_};
    push @list, $_;
    $counter++;
  }

  last if !$counter;

  my $swap = <STDIN>;
  chomp $swap;
  my @swap = split / /, $swap;
  last if ! scalar @swap;

  for my $index (@swap)
  {
    next if $index >= $counter;
    if($index >= $bstart)
    {
      $vidsw{$list[$index]} = $vidsb{$list[$index]};
      delete $vidsb{$list[$index]};
    } else {
      $vidsb{$list[$index]} = $vidsw{$list[$index]};
      delete $vidsw{$list[$index]};
    }
  }
  print "--------------";
}

print "Updating readed list…";

open readed, ">>readed" or die "~/.youtuber/readed?";

for (keys %vidsw)
{
  my $key = $_;
  s/\] /] "/;
  print readed "$_\" ".$vidsw{$key};
}

for (keys %vidsb)
{
  my $key = $_;
  s/\] /] "/;
  print readed "$_\" ".$vidsb{$key};
}

close readed;

download $vidsw{$_} for keys %vidsw;

unlink for </tmp/youtuber/*>;
rmdir "/tmp/youtuber";
